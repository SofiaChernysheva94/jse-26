package ru.t1.chernysheva.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chernysheva.tm.enumerated.Role;
import ru.t1.chernysheva.tm.util.TerminalUtil;

public class UserUpdateProfileCommand extends AbstractUserCommand {

    @NotNull
    private final String NAME = "update-user-profile";

    @NotNull
    private final String DESCRIPTION = "Update profile of current user.";

    @Override
    public void execute() {
        final String userId = getAuthService().getUserId();
        System.out.println("[USER UPDATE PROFILE]");
        System.out.println("FIRST NAME: ");
        @Nullable final String firstName = TerminalUtil.nextLine();
        System.out.println("LAST NAME: ");
        @Nullable final String lastName = TerminalUtil.nextLine();
        System.out.println("MIDDLE NAME: ");
        @Nullable final String middleName = TerminalUtil.nextLine();
        System.out.println("EMAIL: ");
        @Nullable final String email = TerminalUtil.nextLine();
        getUserService().updateUser(userId, firstName, lastName, middleName, email);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }
}
